# The Scout Messages Package
### A centralized package for all system-wide SCOUT message definitions

## Contents:
- BatteryStatus:
	- Used by the `battery_sensor` node to communicate the state of SCOUT's battery to the Central System
	- Format:
		- `level`: An integer in the range [0,100] representing the charge percentage of the battery
		- `is_charging`: (Boolean) Indicates whether battery level has increased since the last update
