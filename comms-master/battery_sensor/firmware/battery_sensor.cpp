/*
 * Battery sensor publisher
 */

#include <ros.h>
#include <scout_msgs/BatteryStatus.h>

#include <Arduino.h>

/*Constant definitions*/

#define BATTERY_MAX 4.2f
#define BATTERY_MIN 3.0f

#define VOLT_TEST_TRIALS 500
#define BATTERY_PIN A2

#define VOLT_ADJ_PARAM_1 39.5f
#define VOLT_ADJ_PARAM_2 2.89f
#define VOLT_ADJ_PARAM_3 89.0f

/*ROS objects setup*/

ros::NodeHandle nh;

scout_msgs::BatteryStatus status_msg;
ros::Publisher statusTopic("battery_status", &status_msg);

/*Sketch Method definitions*/

float readAveragedRawVolatage() {
    float total = 0.0f;
    for (int i=0; i<VOLT_TEST_TRIALS; i++) {
        total += ((float)analogRead(BATTERY_PIN))/1024.0f;
    }

    return ((total/VOLT_TEST_TRIALS)*55.0f);
}

float interpretVoltage(float volts) {
    float calced = VOLT_ADJ_PARAM_1 * log(volts - VOLT_ADJ_PARAM_2) + VOLT_ADJ_PARAM_3;

    return (int)calced;
}

/*Sketch variable definitions*/

int lastRead = 0;

/*Arduino setup*/

void setup() {
    nh.initNode();
    nh.advertise(statusTopic);
}

/*Arduino main*/

void loop() {
    float avRawVoltage = readAveragedRawVolatage();

    int levelParsed = interpretVoltage(avRawVoltage);

    status_msg.level = levelParsed;
    status_msg.is_charging = (levelParsed - lastRead) > 1;

    lastRead = levelParsed;

    statusTopic.publish( &status_msg );
    nh.spinOnce();
    delay(10000);
}

