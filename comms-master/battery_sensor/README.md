# Battery Sensor Package
### Firmware for Battery Status Sensing to be uploaded onto the Comms Arduino

## Upload instructions
- Make the workspace using `catkin_make`
- Generate the arduino packages by running `rosrun rosserial_arduino make_libraries.py $(ARDUINO_LIB_DIR)`
	- `$(ARDUINO_LIB_DIR)` is the location of the Arduino libraries folder (usually `~/Arduino/libraries`)
	- You may have to delete the `ros_lib` directory from the libraries folder first
- Copy the libraries to the package build folder (`cp -r $(ARDUINO_LIB_DIR)/ros_lib $(CATKIN_WS_DIR)/build/battery_sensor`)
- Rebuild the workspace (`catkin_make`)
- Plug in the Arduino
- Upload the sketch using `catkin_make battery_sensor_firmware_bsensor-upload`
- Start the `roscore` service and then start the service using `roslaunch battery_sensor launch_node.launch`
	- **NOTE**: Both the upload command and the roslaunch expect the Arduino to be connected at port `/dev/ttyACM0`. 
	  If this is not the case, the relevant lines in `firmware/CMakeLists.txt` and `launch/launch_node.launch` should be updated
